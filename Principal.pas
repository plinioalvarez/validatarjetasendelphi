﻿unit Principal;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Layouts,
  FMX.ZMaterialEdit, FMX.StdCtrls, FMX.Controls.Presentation, FMX.Edit;

type
  TFPrincipal = class(TForm)
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    GroupBox1: TGroupBox;
    Button1: TButton;
    Label1: TLabel;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    function comprobarTarjeta(C: string): Integer;
  public
    { Public declarations }
  end;

var
  FPrincipal: TFPrincipal;

implementation

const
    TipoTarjeta : Array[0..4] of string = ('No válida', 'Amex', 'Visa', 'Mastercard', 'Discover');

{$R *.fmx}

procedure TFPrincipal.Button1Click(Sender: TObject);
var Tarjeta : string;
begin
   Tarjeta := edit1.Text+edit1.Text+edit1.Text+edit1.Text;
   Label1.Text := TipoTarjeta[ ComprobarTarjeta(Tarjeta)];
end;

function TFPrincipal.comprobarTarjeta(C: string): Integer;
var Card  : string[21];
    VCard : array [0..21] of Byte absolute Card;
    XCard : Integer;
    Cstr  : string[21];
    y, x  : Integer;
begin
  Cstr := '';
  FillChar(VCard, 22, #0);
  Card := C;
  for x := 1 to 20 do
    if (VCard[x] in [48..57]) then
      Cstr := Cstr+Chr(VCard[x]);
  Card := '';
  Card := Cstr;
  XCard := 0;
  if not odd(Length(Card)) then
    for x := (Length(Card)-1) downto 1 do
    begin
      if odd(x) then
        y := ((VCard[x]-48)*2)
      else
        y := (VCard[x]-48);
      if (y>=10) then
        y := ((y-10)+1);
      XCard := (XCard+y)
    end
  else
    for x := (Length(Card)-1) downto 1 do
    begin
      if odd(x) then
        y := (VCard[x]-48)
      else
        y := ((VCard[x]-48)*2);
      if (y>=10) then
        y := ((y-10)+1);
      XCard := (XCard+y)
    end;
    x := (10-(XCard mod 10));
  if (x=10) then
    x := 0;
  if (x=(VCard[Length(Card)]-48)) then
    comprobarTarjeta := Ord(Cstr[1])-Ord('2')
  else
    comprobarTarjeta := 0
end;

end.
